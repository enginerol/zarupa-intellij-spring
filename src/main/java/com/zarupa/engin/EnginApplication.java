package com.zarupa.engin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EnginApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnginApplication.class, args);
    }

}
